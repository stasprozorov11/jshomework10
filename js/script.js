const tabs = document.getElementsByClassName('tabs-title');
let tabMenu;

window.onload=function() {
    tabMenu = document.querySelectorAll('.tabs-content');
    console.log(tabMenu);

    hideTabsContent(1);
}
function hideTabsContent(a) {
    for (let i = a; i < tabMenu.length; i++) {
        tabMenu[i].classList.remove('show');
        tabMenu[i].classList.add("hide");
    }
}

const ulTabs = document.querySelector('.tabs')
ulTabs.addEventListener("click", (event) => {
    let target=event.target;
    if (target.className === 'tabs-title') {
        for (let i = 0; i < tabs.length; i++) {
            if (target === tabs[i]) {
                showTabsContent(i);
                break;
            }
        }
    }

});

function showTabsContent(b){
    if (tabMenu[b].classList.contains('hide')) {
        hideTabsContent(0);
        tabMenu[b].classList.remove('hide');
        tabMenu[b].classList.add('show');
    }
}